# ics-ans-samba

Ansible playbook to install samba.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
