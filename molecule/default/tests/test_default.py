import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('samba')


def test_smb(host):
    smb = host.service("smb")
    assert smb.is_enabled
